from birdnoise.db import get_db

import time

backend = "sqlite"

# register a user
def register_user(first_name, last_name, email, password_hash):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'INSERT INTO user (email, password, first_name, last_name) VALUES (%s,%s,%s,%s)',
        (email, password_hash,first_name, last_name))
    get_db().commit()

# return user based on email
def get_user_by_email(email):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT * FROM user WHERE email = %s', (email,)
        )
    return db.fetchone()

# return user based on user id
def get_user_by_id(user_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT * FROM user WHERE id = %s', (user_id,)
        )
    return db.fetchone()

# returns all posts and reposts combined with a union
# when a post is NOT a repost, repost_id is null
def get_all_posts_and_reposts():
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT '
        'p.id as post_id, '
        'r.id as repost_id, '
        'r.time as post_time '
        'FROM repost r '
        'INNER JOIN post p ON r.post_id=p.id '
        'UNION '
        'SELECT '
        'p.id as post_id, '
        'NULL as repost_id, '
        'p.time as post_time '
        'FROM post p '
        'ORDER BY post_time DESC',
        )
    return db.fetchall()

# returns all posts and reposts like the previous
# function but filtered to only users followed by user_id
def get_all_following_posts(user_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT following_id '
        'FROM follower '
        'WHERE follower_id=%s',
        (user_id,))
    followlist = [f['following_id'] for f in db.fetchall()]
    followlist.append(user_id)
    print(followlist)
    query_string = \
        'SELECT '\
        'p.id as post_id, ' \
        'r.id as repost_id, ' \
        'r.time as post_time ' \
        'FROM repost r ' \
        'INNER JOIN post p ON r.post_id=p.id ' \
        'WHERE r.user_id IN (%s) ' \
        'UNION ALL ' \
        'SELECT ' \
        'p.id as post_id, ' \
        'NULL as repost_id, ' \
        'p.time as post_time ' \
        'FROM post p ' \
        'WHERE p.user_id IN (%s) ' \
        'ORDER BY post_time DESC' \
        % (','.join(["%s"] * len(followlist)),','.join(["%s"] * len(followlist)))

    db.execute(query_string,
        followlist + followlist)
    return db.fetchall()

# get the posts and reposts of a specific user_id
def get_user_posts_and_reposts(user_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT '
        'p.id as post_id, '
        'p.user_id as post_user, '
        'op.first_name as first_name, '
        'op.last_name as last_name, '
        'r.time as post_time, '
        'p.content as content, '
        'r.user_id as reposter, '
        'rep.first_name as r_first_name, '
        'rep.last_name as r_last_name '
        'FROM post p '
        'INNER JOIN repost r ON r.post_id=p.id '
        'INNER JOIN user rep ON rep.id=r.user_id '
        'INNER JOIN user op ON op.id=p.user_id '
        'WHERE r.user_id=%s '
        'UNION '
        'SELECT '
        'p.id as post_id, '
        'p.user_id as post_user, '
        'u.first_name as first_name, '
        'u.last_name as last_name, '
        'p.time as post_time, '
        'p.content as content, '
        'null as reposter, '
        'null as r_first_name, '
        'null as r_last_name '
        'FROM post p '
        'INNER JOIN user u ON u.id=p.user_id '
        'WHERE p.user_id=%s '
        'ORDER BY post_time DESC',
        (user_id, user_id))
    return db.fetchall()

# get a single post
def get_post(post_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT '
        'p.id as post_id, '
        'p.user_id as post_user, '
        'u.first_name as first_name, '
        'u.last_name as last_name, '
        'p.time as post_time, '
        'p.content as content '
        'FROM post p '
        'INNER JOIN user u ON u.id=p.user_id '
        'WHERE p.id=%s',
        (post_id,))
    return db.fetchone()

# get a single repost
def get_repost(repost_id):
    db = get_db().cursor(dictionary=True,buffered=True)
    db.execute(
        'SELECT '
        'p.id as post_id, '
        'p.user_id as post_user, '
        'u.first_name as first_name, '
        'u.last_name as last_name, '
        'r.user_id as r_user, '
        'rep.first_name as r_first_name, '
        'rep.last_name as r_last_name, '
        'r.time as post_time, '
        'p.content as content '
        'FROM repost r '
        'INNER JOIN post p ON p.id=r.post_id '
        'INNER JOIN user u ON u.id=p.user_id '
        'INNER JOIN user rep ON rep.id=r.user_id '
        'WHERE r.id=%s',
        (repost_id,))
    return db.fetchone()

# create a post for user_id containing content
def make_post(user_id, content):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'INSERT INTO post (user_id, time, content) '
        'VALUES (%s,%s,%s)',
        (user_id, int(time.time()), content))
    get_db().commit()

# delete a post, also deletes all comments on it and reposts of it
def delete_post(id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'DELETE FROM post '
        'WHERE post.id=%s',
        (id,))
    db.execute(
        'DELETE FROM comment '
        'WHERE post_id=%s',
        (id,))
    db.execute(
        'DELETE FROM `like` '
        'WHERE post_id=%s',
        (id,))
    db.execute(
        'DELETE FROM `repost` '
        'WHERE post_id=%s',
        (id,))
    get_db().commit()

# creata repost of post_id under user user_id
def make_repost(user_id, post_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'INSERT INTO repost (user_id, post_id, time) '
        'VALUES (%s,%s,%s)',
        (user_id, post_id, int(time.time())))
    get_db().commit()

# create a comment on post_id under user_id with content
def make_comment(user_id, post_id, content):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'INSERT INTO comment (user_id, post_id, time, content) '
        'VALUES (%s,%s,%s,%s)',
        (user_id, post_id, int(time.time()), content))
    get_db().commit()

# like post_id with user_id
def make_like(user_id, post_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'INSERT INTO `like`(user_id, post_id) '
        'VALUES (%s,%s)',
        (user_id, post_id))
    get_db().commit()

# remove a like from post_id with user_id
def remove_like(user_id, post_id):
    db = get_db().cursor(dictionary=True)
    db.execute('DELETE FROM `like` '
               'WHERE user_id=%s AND post_id=%s',
               (user_id, post_id))
    get_db().commit()

# returns whether or not user_id has liked post_id as a bool
def get_like(user_id, post_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT 1 '
        'FROM `like` '
        'WHERE user_id=%s AND post_id=%s',
        (user_id, post_id))
    return db.fetchone() != None

# get a single comment
def get_comment(id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT * '
        'FROM `comment` '
        'WHERE id=%s',
        (id,))
    return db.fetchone()

# return whether follower is following following
def get_follow(follower, following):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT 1 '
        'FROM `follower` '
        'WHERE follower_id=%s AND following_id=%s',
        (follower, following))
    return db.fetchone() != None

# get the number of likes on post_id
def get_likes(post_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT COUNT(user_id) as like_count '
        'FROM `like` '
        'WHERE post_id=%s',
        (post_id,))
    return db.fetchone()['like_count']

# make a comment on post_id under user_id with content
def make_comment(user_id, post_id, content):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'INSERT INTO `comment` (user_id, post_id, time, content) '
        'VALUES (%s,%s,%s,%s)',
        (user_id, post_id, int(time.time()), content))
    get_db().commit()

# delete comment with comment_id
def comment_delete(comment_id):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'DELETE FROM `comment` '
        'WHERE id=%s',
        (comment_id,))
    get_db().commit()

# get all comments on post id (start and end not used)
def get_comments(post_id, start, end):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'SELECT '
        'c.id as id, '
        'c.user_id as user_id, '
        'c.post_id as post_id, '
        'c.time as comment_time, '
        'c.content as content '
        'FROM `comment` c '
        'WHERE c.post_id=%s',
        (post_id,))
    return db.fetchall()

# make follower follow following
def make_follow(following, follower):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'INSERT INTO `follower` '
        '(follower_id, following_id) '
        'VALUES (%s,%s)',
        (follower,following))
    get_db().commit()

# unfollow follower from following
def remove_follow(following, follower):
    db = get_db().cursor(dictionary=True)
    db.execute(
        'DELETE FROM `follower` '
        'WHERE follower_id=%s '
        'AND following_id=%s',
        (follower,following))
    get_db().commit()
