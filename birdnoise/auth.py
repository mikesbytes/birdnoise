import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
    )
from werkzeug.security import check_password_hash, generate_password_hash

from birdnoise.db import get_db
from birdnoise.querier import register_user, get_user_by_email, get_user_by_id

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/register', methods=('GET','POST'))
def register():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        db = get_db()
        error = None

        if not email:
            error = 'Email is required'
        elif not password:
            error = 'Password is required'
        elif get_user_by_email(email) is not None:
            error = '{} is already registered.'.format(email)

        if error is None:
            register_user(first_name, last_name, email, generate_password_hash(password))
            return redirect(url_for('auth.login'))

        flash(error)

    return render_template('auth/register.html')

@bp.route('/login', methods=('GET','POST'))
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        db = get_db()
        error = None
        user = get_user_by_email(email) 

        if user is None:
            error = "Incorrect Username"
        elif not check_password_hash(user['password'], password):
            error = "Incorrect Password"

        if error is None:
            session.clear()
            session['user_id'] = user['id']
            session['user_name'] = user['first_name']
            if 'url' in session:
                return redirect(session['url'])
            return redirect(url_for('index'))

        flash(error)

    return render_template('auth/login.html')

@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = get_user_by_id(user_id)

@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        return view(**kwargs)
    return wrapped_view
